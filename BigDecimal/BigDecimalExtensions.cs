﻿﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace BestingIT.BigDecimal
{
    public static class BigDecimalExtensions
    {

        public static BigDecimal Pow(this BigDecimal d, int exp)
        {
            if (d.N == 0)
                return new BigDecimal(0);
            if (exp == 0)
                return new BigDecimal(1);
            if (exp > 0)
                return new BigDecimal(BigInteger.Pow(d.N, exp), BigInteger.Pow(d.D, exp));
            else
                return new BigDecimal(BigInteger.Pow(d.D, -exp), BigInteger.Pow(d.N, -exp));
        }

    }
}
