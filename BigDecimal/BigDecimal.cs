﻿/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

using System;
using System.Globalization;
using System.Numerics;
using System.Text;

namespace BestingIT.BigDecimal
{

    /// <summary>
    /// BigDecimal represents a decimal number with arbitrary precision.
    /// It is based on a fraction using two BigInteger types.
    /// 
    /// BigDecimal supports parsing, culture invariant string output, type conversions and explicit and implicit casts.
    /// All standard operators for basic operations and comparisons are implemented as well.
    /// 
    /// BigDecimal is easily extensible using the public numerator (N) and denumerator (D) properties for adding 
    /// complex math operations.
    /// </summary>
    public class BigDecimal : IComparable, IComparable<BigDecimal>, IEquatable<BigDecimal>, IConvertible
    {
        private const int MaxDoubleDecimalPlaces = 339;
        public static readonly BigDecimal Zero = new BigDecimal();
        public static readonly BigDecimal One = new BigDecimal(1);

        public BigInteger N { get; set; } // numerator
        public BigInteger D { get; set; } // denominator

        public BigDecimal() { N = BigInteger.Zero; D = BigInteger.One; }

        public BigDecimal(double value) => Parse(value.ToString(GetFormat(), CultureInfo.InvariantCulture));

        public BigDecimal(decimal value) => Parse(value.ToString(GetFormat(), CultureInfo.InvariantCulture));

        string GetFormat() => "0." + new string('#', MaxDoubleDecimalPlaces);

        public BigDecimal(int value) : this(value, 1) { }

        public BigDecimal(long value) : this(value, 1) { }

        public BigDecimal(ulong value) : this(value, 1) { }

        public BigDecimal(BigInteger value) : this(value, 1) { }

        public BigDecimal(string s) => Parse(s);

        public BigDecimal(BigInteger numerator, BigInteger denominator)
        {
            N = numerator;
            D = denominator;
        }

        public BigDecimal(BigDecimal value) : this(value.N, value.D) { }

        void Parse(string v)
        {
            int i = v.IndexOf('.');
            if (i == -1)
            {
                N = BigInteger.Parse(v);
                D = BigInteger.One;
                return;
            }
            N = BigInteger.Parse(v.Replace(".", ""));
            D = BigInteger.Pow(10, v.Length - i - 1);
            Reduce();
        }

        /// <summary>
        /// Best effort conversion to decimal (decimal places are truncated if needed).
        /// </summary>
        /// <returns></returns>
        public decimal ToDecimal()
        {
            var tmp = new BigDecimal(this);
            while (tmp.N > (BigInteger)Decimal.MaxValue 
                || tmp.D > (BigInteger)Decimal.MaxValue
                || tmp.N < (BigInteger)Decimal.MinValue
                || tmp.D < (BigInteger)Decimal.MinValue)
            {
                tmp.N /= 10;
                tmp.D /= 10;
            }
            return (decimal)tmp.N / (decimal)tmp.D;
        }

        /// <summary>
        /// Best effort conversion to double (decimal places are truncated if needed).
        /// </summary>
        /// <returns></returns>
        public double ToDouble()
        {
            var tmp = new BigDecimal(this);
            while (tmp.N > (BigInteger)Double.MaxValue
                || tmp.D > (BigInteger)Double.MaxValue
                || tmp.N < (BigInteger)Double.MinValue
                || tmp.D < (BigInteger)Double.MinValue)
            {
                tmp.N /= 10;
                tmp.D /= 10;
            }
            return (double)tmp.N / (double)tmp.D;
        }

        public override string ToString()
        {
            Reduce();
            if (N == 0)
                return "0";
            if (D == 1)
                return N.ToString();
            var sb = new StringBuilder();
            var tmpN = N * 100;
            var tmpD = D * 100;
            var res = BigInteger.DivRem(tmpN, tmpD, out BigInteger rem);
            sb.Append(res.ToString());
            sb.Append(".");
            while (rem > 0)
            {
                tmpD /= 10;
                if (tmpD < 100 && rem < (BigInteger)decimal.MaxValue)
                {                    
                    decimal last = (decimal)rem / (decimal)tmpD;
                    last = Math.Truncate(10 * last) / 10;
                    sb.Append(last.ToString(CultureInfo.InvariantCulture).Replace(".", ""));
                    break;
                }
                res = BigInteger.DivRem(rem, tmpD, out rem);
                sb.Append(res.ToString());
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            return Equals((BigDecimal)obj);
        }

        public override int GetHashCode()
        {
            return N.GetHashCode() + D.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            return CompareTo((BigDecimal)obj);
        }

        public int CompareTo(BigDecimal other)
        {
            var a = N * other.D;
            var b = other.N * D;
            return BigInteger.Compare(a, b);
        }

        public bool Equals(BigDecimal other)
        {
            return CompareTo(other) == 0;
        }

        /// <summary>
        /// Reduces the internal fraction by using the greatest common divisor.
        /// </summary>
        public void Reduce()
        {
            var gcd = BigInteger.GreatestCommonDivisor(D, N);
            N /= gcd;
            D /= gcd;
        }

        #region converters

        public TypeCode GetTypeCode() => TypeCode.Object;

        public object ToType(Type conversionType, IFormatProvider provider) => Convert.ChangeType(ToDecimal(), conversionType);

        public bool ToBoolean(IFormatProvider provider) => Convert.ToBoolean(this);

        public byte ToByte(IFormatProvider provider) => Convert.ToByte(this);

        public char ToChar(IFormatProvider provider) => throw new InvalidCastException("Conversion not supported.");

        public DateTime ToDateTime(IFormatProvider provider) => throw new InvalidCastException("Conversion not supported.");

        public decimal ToDecimal(IFormatProvider provider) => ToDecimal();

        public double ToDouble(IFormatProvider provider) => ToDouble();

        public short ToInt16(IFormatProvider provider) => Convert.ToInt16(this);

        public int ToInt32(IFormatProvider provider) => Convert.ToInt32(this);

        public long ToInt64(IFormatProvider provider) => Convert.ToInt64(this);

        public sbyte ToSByte(IFormatProvider provider) => Convert.ToSByte(this);

        public float ToSingle(IFormatProvider provider) => Convert.ToSingle(this);

        public string ToString(IFormatProvider provider) => ToString();

        public ushort ToUInt16(IFormatProvider provider) => Convert.ToUInt16(this);

        public uint ToUInt32(IFormatProvider provider) => Convert.ToUInt32(this);

        public ulong ToUInt64(IFormatProvider provider) => Convert.ToUInt64(this);

        #endregion

        #region casts

        public static explicit operator byte(BigDecimal value) => (byte)value.ToType(typeof(byte), null);
        
        public static explicit operator sbyte(BigDecimal value) => (sbyte)value.ToType(typeof(sbyte), null);

        public static explicit operator short(BigDecimal value) => (short)value.ToType(typeof(short), null);

        public static explicit operator int(BigDecimal value) => (int)value.ToType(typeof(int), null);

        public static explicit operator long(BigDecimal value) => (long)value.ToType(typeof(long), null);

        public static explicit operator ushort(BigDecimal value) => (ushort)value.ToType(typeof(ushort), null);

        public static explicit operator uint(BigDecimal value) => (uint)value.ToType(typeof(uint), null);

        public static explicit operator ulong(BigDecimal value) => (ulong)value.ToType(typeof(ulong), null);

        public static explicit operator float(BigDecimal value) => (float)value.ToType(typeof(float), null);

        public static explicit operator double(BigDecimal value) => value.ToDouble();

        public static explicit operator decimal(BigDecimal value) => value.ToDecimal();

        public static implicit operator BigDecimal(byte value) => new BigDecimal(value); 

        public static implicit operator BigDecimal(sbyte value) => new BigDecimal(value); 

        public static implicit operator BigDecimal(short value) => new BigDecimal(value); 

        public static implicit operator BigDecimal(int value) => new BigDecimal(value); 

        public static implicit operator BigDecimal(long value) => new BigDecimal(value); 

        public static implicit operator BigDecimal(ushort value) => new BigDecimal(value); 

        public static implicit operator BigDecimal(uint value) => new BigDecimal(value); 

        public static implicit operator BigDecimal(ulong value) => new BigDecimal(value); 

        public static implicit operator BigDecimal(float value) => new BigDecimal(value);

        public static implicit operator BigDecimal(double value) => new BigDecimal(value);

        public static implicit operator BigDecimal(decimal value) => new BigDecimal(value);

        #endregion

        #region operators

        public static BigDecimal operator -(BigDecimal left, BigDecimal right)
        {
            return new BigDecimal(left.N * right.D - left.D * right.N, left.D * right.D);
        }

        public static BigDecimal operator +(BigDecimal left, BigDecimal right)
        {
            return new BigDecimal(left.N * right.D + left.D * right.N, left.D * right.D);
        }

        public static BigDecimal operator *(BigDecimal left, BigDecimal right)
        {
            return new BigDecimal(left.N * right.N, left.D * right.D);
        }

        public static BigDecimal operator /(BigDecimal left, BigDecimal right)
        {
            return new BigDecimal(left.N * right.D, left.D * right.N);
        }

        public static BigDecimal operator ++(BigDecimal value)
        {
            value.N += value.D;
            return value;
        }

        public static BigDecimal operator --(BigDecimal value)
        {
            value.N -= value.D;
            return value;
        }

        public static bool operator !=(BigDecimal left, BigDecimal right)
        {
            return left.CompareTo(right) != 0;
        }

        public static bool operator ==(BigDecimal left, BigDecimal right)
        {
            return left.CompareTo(right) == 0;
        }

        public static bool operator <(BigDecimal left, BigDecimal right)
        {
            return left.CompareTo(right) < 0;
        }

        public static bool operator <=(BigDecimal left, BigDecimal right)
        {
            return left.CompareTo(right) <= 0;
        }

        public static bool operator >(BigDecimal left, BigDecimal right)
        {
            return left.CompareTo(right) > 0;
        }

        public static bool operator >=(BigDecimal left, BigDecimal right)
        {
            return left.CompareTo(right) >= 0;
        }

        #endregion

    }
}
